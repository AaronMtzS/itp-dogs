<?php


namespace Gamma\Dogs\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\RequestInterface;


class Breeds implements ArgumentInterface
{
    protected $request;
    protected $breeds;
    protected $requestedData;

    /**
     *
     */

    public function __construct(
        RequestInterface $request,
        \Gamma\Dogs\Model\Breeds $breeds
    )
    {
        $this->request = $request;
        $this->breeds = $breeds;
    }

    public function getBreeds(): array
    {
        $dogData = $this->breeds->getBreeds();

        return $dogData;

    }

    public function requestDataExist(){
        $data = $this->request->getParams() ? true : false;

        return ($data);
    }

    public function getDog(){
        $name = $this->request->getParam("breed");

        return $this->breeds->getDog($name);
    }

    public function getBooks(){
        $title = $this->request->getParam("breed");

        return $this->breeds->getBooks($title);
    }
}