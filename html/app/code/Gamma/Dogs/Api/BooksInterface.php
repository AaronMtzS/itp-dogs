<?php


namespace Gamma\Dogs\Api;


interface BooksInterface
{
    public function getBooks(string $breed): BooksInterface;
}