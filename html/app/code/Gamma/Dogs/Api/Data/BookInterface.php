<?php


namespace Gamma\Dogs\Api\Data;


interface BookInterface
{
    const NAME = 'name';
    const COVER = 'cover';
    const REVIEW = 'review';
    const DOWNLOAD = 'download';

    public function getName(): string;
    public function setName(string $name): BookInterface;

    public function getCover(): string;
    public function setCover(string $cover): BookInterface;

    public function getReview(): string;
    public function setReview(string $review): BookInterface;

    public function getDownload(): string;
    public function setDownload(string $download): BookInterface;
}