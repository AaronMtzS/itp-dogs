<?php


namespace Gamma\Dogs\Api\Data;

interface DogInterface
{
    const BREED = 'breed';
    const IMAGE = 'image';
    const BOOKS = 'books';
    const SUBBREED = 'subbreed';

    public function getBreed(): string;

    public function setBreed(string $breed): DogInterface;

    public function getImage():string;

    public function setImage(string $image): DogInterface;

    public function getBooks():array ;

    public function setBooks(array $books): DogInterface;

    public function getSubbreed():array ;

    public function setSubbreed(array $subbreed):DogInterface;


}