<?php


namespace Gamma\Dogs\Api;

interface BreedsInterface
{
    public function getBreeds(): array;
}