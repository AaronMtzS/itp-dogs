<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\ConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class Connection implements ConnectionInterface
{
    protected $baseUrl;
    protected $baseUrlBooks;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://dog.ceo/api/breed',
        string $baseUrlBooks = 'https://openlibrary.org/search.json?title='
    )
    {
        $this->baseUrl = $baseUrl;
        $this->baseUrlBooks = $baseUrlBooks;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function get(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrl}{$resourcePath}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }

    public function getBooks(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrlBooks}{$resourcePath}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }
}