<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\BooksInterface;
use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\Data\BookInterfaceFactory;

class Books implements BooksInterface
{
    protected $connection;
    protected $bookFactory;

    public function __construct(
        ConnectionInterface $connection,
        BookInterfaceFactory $bookFactory
    )
    {
        $this->connection = $connection;
        $this->bookFactory = $bookFactory;
    }

    public function getBooks(string $breed): BooksInterface
    {
        $detailedData = $this->connection->getBooks("{$breed}&subject=dog");

        $book = $this->bookFactory->create();


        if(count($detailedData['docs'])>2){
            $books = array($detailedData['docs'][0]['title_suggest'],$detailedData['docs'][1]['title_suggest'],$detailedData['docs'][2]['title_suggest']);
        }else{
            $books = array("Not books found");
        }


        return $books;
    }
}