<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\BreedsInterface;
use Gamma\Dogs\Api\Data\DogInterfaceFactory;
use Symfony\Component\Console\Input\ArrayInputFactory;

class Breeds implements BreedsInterface
{
    protected $connection;
    protected $dogFactory;

    public function __construct(
        ConnectionInterface $connection,
        DogInterfaceFactory $dogFactory
    )
    {
        $this->connection = $connection;
        $this->dogFactory = $dogFactory;
    }

    public function getBreeds(): array
    {
        $dogData = $this->connection->get("s/list/all");

        $dogBreeds = $dogData['message'];

        return $dogBreeds;
    }

    public function getDog(string $breed) {
        $dogData = $this->connection->get("/${breed}/list");

        $dog = $this->dogFactory->create();
        $books = $this->getBooks($breed);
        $image = $this->getImage($breed);

        $dog->setBreed($breed)
            ->setImage($image)
            ->setSubbreed($dogData['message'])
            ->setBooks($books);

        return $dog;
    }

    protected function getImagesData(string $breed): array
    {
        return $this->connection->get("/{$breed}/images/random");
    }

    protected function getBooksData(string $breed): array
    {
        return $this->connection->getBooks("{$breed}&subject=dog");
    }

    protected function getImage(string $breed):string{
        $detailedData = $this->getImagesData($breed);

        return $detailedData['message'];
    }

    public function getBooks(string $breed):array {
        $detailedData = $this->getBooksData($breed);

        if(count($detailedData['docs'])>2){
            $books = array($detailedData['docs'][0]['title_suggest'],$detailedData['docs'][1]['title_suggest'],$detailedData['docs'][2]['title_suggest']);
        }else{
            $books = array("Not books found");
        }


        return $books;
    }
}