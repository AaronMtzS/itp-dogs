<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\DogInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Dog extends AbstractSimpleObject implements DogInterface
{
    public function getBreed(): string
    {
        return $this->_get(self::BREED);
    }

    public function setBreed(string $breed): DogInterface
    {
        return $this->setData(self::BREED, $breed);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): DogInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getBooks(): array
    {
        return $this->_get(self::BOOKS);
    }

    public function setBooks(array $books): DogInterface
    {
        return $this->setData(self::BOOKS, $books);
    }

    public function getSubbreed(): array
    {
        return $this->_get(self::SUBBREED);    }

    public function setSubbreed(array $subbreed): DogInterface
    {
        return $this->setData(self::SUBBREED, $subbreed);
    }
}