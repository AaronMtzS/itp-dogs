<?php


namespace Gamma\Dogs\Model\Data;

use Gamma\Dogs\Api\Data\BookInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Book extends AbstractSimpleObject implements BookInterface
{

    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName(string $name): BookInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function getCover(): string
    {
        return $this->_get(self::COVER);    }

    public function setCover(string $cover): BookInterface
    {
        return $this->setData(self::COVER, $cover);
    }

    public function getReview(): string
    {
        return $this->_get(self::REVIEW);    }

    public function setReview(string $review): BookInterface
    {
        return $this->setData(self::REVIEW, $review);
    }

    public function getDownload(): string
    {
        return $this->_get(self::DOWNLOAD);    }

    public function setDownload(string $download): BookInterface
    {
        return $this->setData(self::DOWNLOAD, $download);
    }
}